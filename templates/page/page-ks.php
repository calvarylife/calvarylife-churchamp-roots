<?php get_template_part('templates/structure/header'); ?>

  <div id="wrap" class="wrap clearfix" role="document">
    <div class="content">

			<?php get_template_part('templates/structure/sidebar'); ?>

				<main id="main" class="main clearfix" role="main">
					<div id="main-container" class="col-xxxl-10 col-xxl-9 col-xl-9 col-lg-9 col-md-12">
					 	<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
								<?php get_template_part('templates/structure/feature'); ?>

													<?php get_template_part('templates/content/content-ks'); ?>

						</article>
					</div><!--/main-container-->
				</main><!--/main-->

    </div><!-- /content -->
    <div class="push"></div>
  </div><!-- /wrap -->

<?php get_template_part('templates/structure/footer'); ?>