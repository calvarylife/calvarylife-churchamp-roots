<?php get_template_part('templates/structure/header'); ?>

  <div id="wrap" class="wrap clearfix" role="document">
    <div class="content">

			<?php get_template_part('templates/structure/sidebar'); ?>

			<?php get_template_part('templates/structure/main'); ?>

    </div><!-- /content -->
  </div><!-- /wrap -->

<?php get_template_part('templates/structure/footer'); ?>