{exp:channel:entries entry_id="{embed:_id'); ?>" dynamic="no" channel="blocks" parse="inward" cache="yes" disable="categories|category_fields|member_data|pagination"'); ?>
<div class="container">
    <?php if(get_field('block_title')) { ?>
                
    <h3><?php the_field('block_title'); ?></h3>
    <?php } ?>
    <?php if(get_field('block_subtitle')) { ?>
    <h4><?php the_field('block_subtitle'); ?></h4>
    <?php } ?>
    <div class="row">
        <?php the_field('block_image_matrix'); ?>
        <div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
            <img class="img-responsive" src="{image_upload'); ?>" alt="{image_alt_text'); ?>">
        </div>

        {if  {row_count'); ?> != {total_rows'); ?> && {exp:audacity:modulo dividend="{row_count'); ?>" divisor="4"'); ?> == 0'); ?>
    </div>
    <div class="row">
        <?php } ?>

        {/block_image_matrix'); ?>
    </div>
</div>
{/exp:channel:entries'); ?>