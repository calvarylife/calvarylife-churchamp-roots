<div class="container">
    <div class="row">
        <div class="table-block col-md-12">
                <div class="cta-container center-block">
                    <?php if(get_field('block_title')) { ?>
                
                    <h3><?php the_field('block_title'); ?></h3>
                    <?php } ?>
                    {exp:freeform:form
                    form_name="call_to_action_block"
                    form:class="form-inline call-to-action-form"
                    ajax="yes"
                    inline_errors="no"
                    required="email"
                    '); ?>

                    {freeform:field:full_name
                    attr:id="cta-full-name"
                    attr:class="cta-field form-control"
                    attr:placeholder="Full Name"'); ?>

                    {freeform:field:email
                    attr:id="cta-email"
                    attr:class="cta-field form-control"
                    attr:placeholder="E-mail address"'); ?>

                    {freeform:field:phone_number
                    attr:id="cta-phone"
                    attr:class="cta-field form-control"
                    attr:placeholder="Phone Number"'); ?>


                    <input type="submit" name="submit" value="submit" class="submit-form-manual" />
                    <i class="fa fa-chevron-right"></i>

                    <!--{freeform:submit-->
                    <!--attr:class="btn btn-primary submit-form-manual"'); ?>-->

                    <div class="callback"></div>
                    {/exp:freeform:form'); ?>
                </div>
        </div>
    </div>
</div>