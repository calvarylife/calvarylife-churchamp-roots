{exp:channel:entries entry_id="{embed:_id'); ?>" dynamic="no" channel="blocks" parse="inward" cache="yes" disable="categories|category_fields|member_data|pagination"'); ?>
<div class="container">
    <?php if(get_field('block_title')) { ?>
                
    <h3><?php the_field('block_title'); ?></h3>
    <?php } ?>
    <?php if(get_field('block_subtitle')) { ?>
    <h4><?php the_field('block_subtitle'); ?></h4>
    <?php } ?>
    <div class="row">
        <div class="table-block col-md-{column_1_width'); ?>">
            <?php the_field('block_text_left'); ?>
        </div>
        <div class="col-md-{column_2_width'); ?>">
            <?php the_field('block_text_right'); ?>
        </div>
    </div>
</div>
{/exp:channel:entries'); ?>