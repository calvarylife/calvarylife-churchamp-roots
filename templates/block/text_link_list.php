{exp:channel:entries entry_id="{embed:_id'); ?>" dynamic="no" channel="blocks" parse="inward" cache="yes" disable="categories|category_fields|member_data|pagination"'); ?>
<div class="container">
    <?php if(get_field('block_title')) { ?>
                
    <h3><?php the_field('block_title'); ?></h3>
    <?php } ?>
    <?php if(get_field('block_subtitle')) { ?>
    <h4><?php the_field('block_subtitle'); ?></h4>
    <?php } ?>
    <div class="row">
        <div class="col-md-6">
            <?php the_field('block_text_left'); ?>
        </div>
        <div class="col-md-6">
            <ul class="link-list">
            <?php the_field('block_link_list'); ?>
                <li class="link-list-item"><a href="{link_url'); ?>" target="_blank">{link_text'); ?></a></li>
            {/block_link_list'); ?>
            </ul>
        </div>
    </div>
</div>
{/exp:channel:entries'); ?>