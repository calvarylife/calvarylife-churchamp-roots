{exp:channel:entries entry_id="{embed:_id'); ?>" dynamic="no" channel="blocks" parse="inward" cache="yes" disable="categories|category_fields|member_data|pagination"'); ?>
<div class="container">
    <style>

        /* Move this to viewport.less */
        .text-overlay {
            position:absolute;
            top:40%;
            margin-left:5%;
            margin-right:5%;
        '); ?>

    </style>
    <div class="row">
        <div class="col-md-12 text-overlay-container">
            <img class="img-responsive" src="<?php the_field('block_image_single'); ?>" alt="full-width-image">
            <div class="text-overlay">
                <?php if(get_field('block_title')) { ?>
                
                <h3><?php the_field('block_title'); ?></h3>
                <?php } ?>
                <?php if(get_field('block_subtitle')) { ?>
                <h4><?php the_field('block_subtitle'); ?></h4>
                <?php } ?>
                <span class="text-overlay-lead"><?php the_field('block_text_full'); ?></span>
            </div>
        </div>
    </div>
</div>
{/exp:channel:entries'); ?>