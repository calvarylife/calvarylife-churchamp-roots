{exp:channel:entries entry_id="{embed:_id'); ?>" dynamic="no" channel="blocks" parse="inward" cache="yes" disable="categories|category_fields|member_data|pagination"'); ?>
<div class="container">
    <?php if(get_field('block_title')) { ?>
                
    <h3><?php the_field('block_title'); ?></h3>
    <?php } ?>
    <?php if(get_field('block_subtitle')) { ?>
    <h4><?php the_field('block_subtitle'); ?></h4>
    <?php } ?>
    <div class="row">
        <div class="col-md-6">
            <?php the_field('block_text_left'); ?>
        </div>
        <div class="col-md-6">
            <div class="panel-group" id="accordion">
            <?php the_field('block_accordion'); ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse-{row_id'); ?>">
                                    <h4 class="panel-title">{accordion_title'); ?></h4></a>
                        </div>
                        <div id="collapse-{row_id'); ?>" class="panel-collapse collapse">
                            <div class="panel-body">
                                {accordion_body'); ?>
                            </div>
                        </div>
                    </div>
            {/block_accordion'); ?>
            </div>

        </div>
    </div>
</div>
{/exp:channel:entries'); ?>