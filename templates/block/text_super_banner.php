{exp:channel:entries entry_id="{embed:_id'); ?>" dynamic="no" channel="blocks" parse="inward" cache="yes" disable="categories|category_fields|member_data|pagination"'); ?>
<div class="super-banner" id="banner-<?php the_ID(); ?>"></div>
<div class="container">
    {exp:stash:set name="_block_header"'); ?>
    {if embed:_first'); ?>
    <h1><?php the_field('block_title'); ?></h1>
    {if:else'); ?>
    <h2><?php the_field('block_title'); ?></h2>
    <?php } ?>
    {if block_subtitle'); ?>
    <h4><?php the_field('block_subtitle'); ?></h4>
    <?php } ?>{/exp:stash:set'); ?>

    {if block_image_single'); ?>
    <div class="icon-wrapper clearfix">
        <div class="icon-container">
            {exp:ce_img:single src="<?php the_field('block_image_single'); ?>" height="90" crop="no" class="icon img-responsive"'); ?>
        </div>
    {exp:stash:get name="_block_header"'); ?>
    </div>
    {if:else'); ?>
    {exp:stash:get name="_block_header"'); ?>
    <?php } ?>
    <p class="lead">
        <?php the_field('block_lead_text'); ?>
    </p>
</div>
{if block_background_image'); ?>
{exp:low_variables:parse var="backstretch_block" preparse:_id="banner-<?php the_ID(); ?>" preparse:_image="<?php the_field('block_background_image'); ?>"'); ?>
<?php } ?>
{/exp:channel:entries'); ?>