<div class="container">
    <div class="row">
        <div class="table-block col-md-12">
            <div class="cta-container center-block clearfix">
                <?php if(get_field('block_title')) { ?>
                <h3><?php the_field('block_title'); ?></h3>
                <?php } ?>
                <a class="btn btn-primary cta-btn" href="<?php the_field('block_button_url'); ?>" target="_blank"><?php the_field('block_button_title'); ?></a>
            </div>
        </div>
    </div>
</div>