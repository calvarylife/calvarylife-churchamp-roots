{exp:channel:entries entry_id="{embed:_id'); ?>" dynamic="no" channel="blocks" parse="inward" cache="yes" disable="categories|category_fields|member_data|pagination"'); ?>
<div class="container">
    <?php if(get_field('block_title')) { ?>
                
    <h3><?php the_field('block_title'); ?></h3>
    <?php } ?>
    <?php if(get_field('block_subtitle')) { ?>
    <h4><?php the_field('block_subtitle'); ?></h4>
    <?php } ?>
    <div class="row">
        <div class="col-md-12">

        <!-- Don't use this slider, it will be replaced by Royal Slider -->

            <!--
                ======= Matrix Fields to use with Royal Slider =======

                <?php the_field('block_slider_image_matrix'); ?>
                <img src="<?php the_field('block_slider_caption'); ?>" alt="<?php the_field('block_slider_alt_text'); ?>">
                {/block_slider_image_matrix'); ?>

                ======= End Matrix Fields =======
            -->


            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel" data-slide-to="1"></li>
                    <li data-target="#carousel" data-slide-to="2"></li>
                </ol>
                <!-- Wrapper for slides -->
                <!-- Need active class on one -->
                <div class="carousel-inner">

                    <?php the_field('block_slider_image_matrix'); ?>
                    <div class="item {if '{field_row_count'); ?>'==1'); ?>active<?php } ?>">
                        <img src="<?php the_field('block_slider_image'); ?>" alt="<?php the_field('block_slider_alt_text'); ?>">
                        <div class="carousel-caption">
                            <?php the_field('block_slider_caption'); ?>
                        </div>
                    </div>
                    {/block_slider_image_matrix'); ?>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?php the_field('block_text_left'); ?>
        </div>
        <div class="col-md-6">
            <?php the_field('block_text_right'); ?>
        </div>
    </div>


</div>
{/exp:channel:entries'); ?>