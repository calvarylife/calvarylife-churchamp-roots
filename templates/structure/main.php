<main id="main" class="main clearfix" role="main">
	<div id="main-container" class="col-xxxl-10 col-xxl-9 col-xl-9 col-lg-9 col-md-12">
	 	<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<?php while (have_posts()) : the_post(); ?>
									<?php get_template_part('templates/structure/feature'); ?>
				<section id="section" class="section">
					<div class="block clearfix">
						<div class="block-row row">
							<div class="block-container col-md-12">
								<div class="container">
									<?php the_content(); ?>
									<?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
								</div><!--/container-->
							</div><!--/block-container-->
						</div><!--/block-row-->
					</div><!--/block-->
				</section><!--/section-->
			<?php endwhile; ?>
		</article>
	</div><!--/main-container-->
</main><!--/main-->