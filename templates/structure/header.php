<header id="site-header">
    <nav id="navbar" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div id="navbar-header" class="navbar-header website-brand-wrap">
                <a href="/" class="navbar-brand website-brand"><img src="/wp-content/themes/calvarylife-ca-roots/assets/img/calvarylife-brand-white.png" class="img-responsive" title="CalvaryLife" alt="CalvaryLife"></a>
                <a data-target="#navbar-collapse-target" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span>Site Navigation</span>
                </a>
            </div> <!--/navbar-header-->
            <div id="navbar-collapse-target" class="navbar-collapse collapse">
                <?php
		        if (has_nav_menu('primary_navigation')) :
		          wp_nav_menu(array(
		          	'theme_location' 	=> 'primary_navigation',
		          	'menu_class' 		=> 'nav navbar-nav',
		          	'menu_id'			=> 'navbar-parent',

		          ));
		        endif;
		      ?>
            </div><!--/navbar-collapse-->
        </div><!--/container-->

    </nav><!--/navbar-->
</header>