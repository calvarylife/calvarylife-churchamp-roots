<aside id="sidebar" class="sidebar clearfix" role="complementary">
	<div id="navbar-header" class="navbar-header">
	    <a data-target="#navbar-collapse-sidebar" data-toggle="collapse" class="navbar-toggle" type="button">
	        <span>Page Navigation</span>
	    </a>
	</div> <!--/navbar-header-->
	<div id="navbar-collapse-sidebar" class="navbar-collapse collapse">
	    <div id="sidebar-container" class="sidebar-container col-xxxl-2 col-xxl-3 col-xl-3 col-lg-3 clearfix">
	        <div id="sidebar-content" data-spy="affix" data-offset-top="0" class="sidebar-content">
	        	<div class="sidebar-content-wrap">
      				<div id="sidebar-header" class="sidebar-header website-brand-wrap">
      					<a href="/" class="navbar-brand website-brand"><img src="/wp-content/themes/calvarylife-ca-roots/assets/img/calvarylife-brand-white.png" class="img-responsive" title="CalvaryLife" alt="CalvaryLife"></a>
      				</div>
              <nav class="nav breadcrumb-links">
              <!--
              		<div class="bc-parent-1">
              			<a href="#">SUB-DOMAIN</a>
              		</div>
              		-->
                  <div class="bc-current">
                      <a href="#">SUB-DOMAIN</a> / <span>CURRENT PAGE</span>
                  </div>
              </nav>
              <nav class="nav section-links">
                  <ul class="list-sections content">
                  	<li><a href="#overview">Overview</a></li>
                  	<li><a href="#section">Section</a></li>
                  	<li><a href="#text_super">Text Super</a></li>
                  </ul>
              </nav>
              <nav class="nav sibling-links">
                  <ul class="list-pages content">
                  	<li><a href="#">Link</a></li>
                  </ul>
              </nav>
	        	</div><!--/sidebar-content-wrap-->
	        </div><!--/sidebar-content-->
	    </div><!--/sidebar-container-->
	</div><!--/navbar-collapse-->
</aside><!-- sidebar -->