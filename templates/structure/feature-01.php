<section id="section" class="section">
	<div class="block clearfix colored text_super first_block">
		<div class="block-row row">
			<div class="block-container col-md-12">
				<div class="container">
					<header class="block-header-plus clearfix">
					    <div class="block-icon">
					    	<i class="icon img-responsive fa fa-bookmark">&nbsp;</i>
					    </div>
					    <div class="block-headline">
					    	<h1 class="block-title-plus"><?php echo roots_title(); ?></h1>
								<p class="block-subtitle-plus">Subtitle Goes Here</p>
					    </div>
					</header>
				</div><!--/container-->
			</div><!--/block-container-->
		</div><!--/block-row-->
	</div><!--/block-->
</section><!--/section-->