<!-- FOOTER -->
<footer id="site-footer" class="footer">
    <div class="container">
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 col-ts-12">
            <ul class="nav nav-pills pull-left">
                <li><span>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></span></li>
            </ul>
        </div><!--/col-->
        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 col-ts-12">
            <ul class="nav nav-pills pull-right">
                <li><a href="/privacy-policy">Privacy Policy</a></li>
                <li><a href="/contact" title="Contact">Contact</a></li>
                <li><a href="/sitemap" title="Site Map">Site Map</a></li>
                <li><a class="link-top" href="#top">Top</a></li>
            </ul>
        </div><!--/col-->
    </div><!--/container-->
</footer>

<?php wp_footer(); ?>