<section id="overview" class="section">
	<div class="block clearfix colored text_super_banner first_block">
		<div class="row block-row">
			<div class="block-container col-md-12">
				<div class="super-banner" id="page-super-banner"></div>
				<div class="container">
					<header class="block-header-plus clearfix">
					    <div class="block-icon">
					    	<i class="icon img-responsive fa fa-bookmark">&nbsp;</i>
					    </div>
					    <div class="block-headline">
					    	<h1 class="block-title-plus"><?php echo roots_title(); ?></h1>
								<p class="block-subtitle-plus">Subtitle Goes Here</p>
					    </div>
					</header>
					<p class="lead">
					This is lead copy Sed eleifund metus ut nisi sollicitudin eusmod a in metus. Aliquam sed mi porta mi congue ullamcorper. Phasellus rutrum lectus sit amet nisl faucibus adipiscing aoluptat voluptat.
					</p>
				</div><!--/block-container-->
				<script type="text/javascript">
				$("#page-super-banner").backstretch("/wp-content/themes/calvarylife-ca-roots/assets/img/cl-wc.jpg");
				</script>
			</div><!--/block-container-->
		</div><!--/block-row-->
	</div><!--/block-->
</section><!--/section-->