<section id="text_link_list" class="section">
<span class="label label-default">text_link_list</span>
<div class="block clearfix gray text_link_list">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<div class="link-list">
<a class="link-list-item" href="http://www.google.com" target="_blank">Google</a>
<a class="link-list-item" href="http://www.calvarylife.cc" target="_blank">Calvary Church</a>
<a class="link-list-item" href="http://www.google.com" target="_blank">Google</a>
<a class="link-list-item" href="http://www.calvarylife.cc" target="_blank">Calvary Church</a>
<a class="link-list-item" href="http://www.google.com" target="_blank">Google</a>
<a class="link-list-item" href="http://www.calvarylife.cc" target="_blank">Calvary Church</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_link_list</span>
<div class="block clearfix colored text_link_list">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<div class="link-list">
<a class="link-list-item" href="http://www.google.com" target="_blank">Google</a>
<a class="link-list-item" href="http://www.calvarylife.cc" target="_blank">Calvary Church</a>
<a class="link-list-item" href="http://www.google.com" target="_blank">Google</a>
<a class="link-list-item" href="http://www.calvarylife.cc" target="_blank">Calvary Church</a>
<a class="link-list-item" href="http://www.google.com" target="_blank">Google</a>
<a class="link-list-item" href="http://www.calvarylife.cc" target="_blank">Calvary Church</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_link_list</span>
<div class="block clearfix default text_link_list">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<div class="link-list">
<a class="link-list-item" href="http://www.google.com" target="_blank">Google</a>
<a class="link-list-item" href="http://www.calvarylife.cc" target="_blank">Calvary Church</a>
<a class="link-list-item" href="http://www.google.com" target="_blank">Google</a>
<a class="link-list-item" href="http://www.calvarylife.cc" target="_blank">Calvary Church</a>
<a class="link-list-item" href="http://www.google.com" target="_blank">Google</a>
<a class="link-list-item" href="http://www.calvarylife.cc" target="_blank">Calvary Church</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section id="text_image_top" class="section">
<span class="label label-default">text_image_top</span>
<div class="block clearfix gray text_image_top">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-12">
<img class="img-responsive" src="http://placehold.it/1131x250" alt="image-top" width="1140" height="252" pagespeed_url_hash="351548600"/>
</div>
</div>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_image_top</span>
<div class="block clearfix colored text_image_top">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-12">
<img class="img-responsive" src="http://placehold.it/1131x250" alt="image-top" width="1140" height="252" pagespeed_url_hash="351548600"/>
</div>
</div>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_image_top</span>
<div class="block clearfix default text_image_top">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-12">
<img class="img-responsive" src="http://placehold.it/1131x250" alt="image-top" width="1140" height="252" pagespeed_url_hash="351548600"/>
</div>
</div>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section id="text_image_right" class="section">
<span class="label label-default">text_image_right</span>
<div class="block clearfix gray text_image_right">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Text with image or a graph on the right</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<img class="img-responsive" src="http://placehold.it/550x400" alt="image-right" width="540" height="393" pagespeed_url_hash="2022923344"/>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_image_right</span>
<div class="block clearfix colored text_image_right">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Text with image or a graph on the right</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<img class="img-responsive" src="http://placehold.it/550x400" alt="image-right" width="540" height="393" pagespeed_url_hash="2022923344"/>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_image_right</span>
<div class="block clearfix default text_image_right">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Text with image or a graph on the right</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<img class="img-responsive" src="http://placehold.it/550x400" alt="image-right" width="540" height="393" pagespeed_url_hash="2022923344"/>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section id="text_image_left" class="section">
<span class="label label-default">text_image_left</span>
<div class="block clearfix gray text_image_left">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Text with an image or graph on the left</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-6">
<img class="img-responsive" src="http://placehold.it/550x400" alt="image-left" width="540" height="393" pagespeed_url_hash="2022923344"/>
</div>
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_image_left</span>
<div class="block clearfix colored text_image_left">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Text with an image or graph on the left</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-6">
<img class="img-responsive" src="http://placehold.it/550x400" alt="image-left" width="540" height="393" pagespeed_url_hash="2022923344"/>
</div>
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_image_left</span>
<div class="block clearfix default text_image_left">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Text with an image or graph on the left</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-6">
<img class="img-responsive" src="http://placehold.it/550x400" alt="image-left" width="540" height="393" pagespeed_url_hash="2022923344"/>
</div>
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section id="text_column_two" class="section">
<span class="label label-default">text_column_two</span>
<div class="block clearfix gray text_column_two">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_column_two</span>
<div class="block clearfix colored text_column_two">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_column_two</span>
<div class="block clearfix default text_column_two">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section id="text_column_three" class="section">
<span class="label label-default">text_column_three</span>
<div class="block clearfix gray text_column_three">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-4">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-4">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-4">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_column_three</span>
<div class="block clearfix colored text_column_three">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-4">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-4">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-4">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_column_three</span>
<div class="block clearfix default text_column_three">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-4">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-4">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-4">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section id="text_column_one" class="section">
<span class="label label-default">text_column_one</span>
<div class="block clearfix gray text_column_one">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-12">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_column_one</span>
<div class="block clearfix colored text_column_one">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-12">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_column_one</span>
<div class="block clearfix default text_column_one">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-12">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section id="text_accordion" class="section">
<span class="label label-default">text_accordion</span>
<div class="block clearfix gray text_accordion">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<div class="panel-group" id="accordion-10">
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion-10" href="#collapse-10-214" class="collapsed">
<h4 class="panel-title">Accordion title 1</h4></a>
</div>
<div id="collapse-10-214" class="panel-collapse collapse">
<div class="panel-body">
Accordion body 1
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion-10" href="#collapse-10-215" class="collapsed">
<h4 class="panel-title">Accordion title 2</h4></a>
</div>
<div id="collapse-10-215" class="panel-collapse collapse">
<div class="panel-body">
Accordion body 2
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion-10" href="#collapse-10-216" class="collapsed">
<h4 class="panel-title">Accordion title 3</h4></a>
</div>
<div id="collapse-10-216" class="panel-collapse collapse">
<div class="panel-body">
Accordion body 3
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_accordion</span>
<div class="block clearfix colored text_accordion">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<div class="panel-group" id="accordion-11">
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion-11" href="#collapse-11-214" class="collapsed">
<h4 class="panel-title">Accordion title 1</h4></a>
</div>
<div id="collapse-11-214" class="panel-collapse collapse">
<div class="panel-body">
Accordion body 1
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion-11" href="#collapse-11-215" class="collapsed">
<h4 class="panel-title">Accordion title 2</h4></a>
</div>
<div id="collapse-11-215" class="panel-collapse collapse">
<div class="panel-body">
Accordion body 2
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion-11" href="#collapse-11-216" class="collapsed">
<h4 class="panel-title">Accordion title 3</h4></a>
</div>
<div id="collapse-11-216" class="panel-collapse collapse">
<div class="panel-body">
Accordion body 3
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_accordion</span>
<div class="block clearfix default text_accordion">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<div class="panel-group" id="accordion-12">
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion-12" href="#collapse-12-214" class="collapsed">
<h4 class="panel-title">Accordion title 1</h4></a>
</div>
<div id="collapse-12-214" class="panel-collapse collapse">
<div class="panel-body">
Accordion body 1
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion-12" href="#collapse-12-215" class="collapsed">
<h4 class="panel-title">Accordion title 2</h4></a>
</div>
<div id="collapse-12-215" class="panel-collapse collapse">
<div class="panel-body">
Accordion body 2
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion-12" href="#collapse-12-216" class="collapsed">
<h4 class="panel-title">Accordion title 3</h4></a>
</div>
<div id="collapse-12-216" class="panel-collapse collapse">
<div class="panel-body">
Accordion body 3
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section class="section section_hidden">
<span class="label label-default">faq_accordion</span>
<div class="block clearfix default faq_accordion">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">FAQ</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-12">
<p><h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p></p>
</div>
<div class="col-md-12">
<div class="panel-group" id="accordion-274">
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse-274-268-acc-1" class="collapsed">
<h4 class="panel-title">Who ate all of my candy?</h4>
</a>
</div>
<div id="collapse-274-268-acc-1" class="panel-collapse collapse">
<div class="panel-body">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse-274-269-acc-1" class="collapsed">
<h4 class="panel-title">Why doesn't my stomach feel very good?</h4>
</a>
</div>
<div id="collapse-274-269-acc-1" class="panel-collapse collapse">
<div class="panel-body">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse-274-270-acc-1" class="collapsed">
<h4 class="panel-title">A third FAQ question?</h4>
</a>
</div>
<div id="collapse-274-270-acc-1" class="panel-collapse collapse">
<div class="panel-body">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse-274-271-acc-1" class="collapsed">
<h4 class="panel-title">Who ate all of my candy?</h4>
</a>
</div>
<div id="collapse-274-271-acc-1" class="panel-collapse collapse">
<div class="panel-body">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse-274-272-acc-1" class="collapsed">
<h4 class="panel-title">Why doesn't my stomach feel very good?</h4>
</a>
</div>
<div id="collapse-274-272-acc-1" class="panel-collapse collapse">
<div class="panel-body">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse-274-273-acc-1" class="collapsed">
<h4 class="panel-title">A third FAQ question?</h4>
</a>
</div>
<div id="collapse-274-273-acc-1" class="panel-collapse collapse">
<div class="panel-body">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">faq_accordion</span>
<div class="block clearfix gray faq_accordion">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">FAQ</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-12">
<p><h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p></p>
</div>
<div class="col-md-12">
<div class="panel-group" id="accordion-274">
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse-274-268-acc-2" class="collapsed">
<h4 class="panel-title">Who ate all of my candy?</h4>
</a>
</div>
<div id="collapse-274-268-acc-2" class="panel-collapse collapse">
<div class="panel-body">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse-274-269-acc-2" class="collapsed">
<h4 class="panel-title">Why doesn't my stomach feel very good?</h4>
</a>
</div>
<div id="collapse-274-269-acc-2" class="panel-collapse collapse">
<div class="panel-body">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse-274-270-acc-2" class="collapsed">
<h4 class="panel-title">A third FAQ question?</h4>
</a>
</div>
<div id="collapse-274-270-acc-2" class="panel-collapse collapse">
<div class="panel-body">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse-274-271-acc-2" class="collapsed">
<h4 class="panel-title">Who ate all of my candy?</h4>
</a>
</div>
<div id="collapse-274-271-acc-2" class="panel-collapse collapse">
<div class="panel-body">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse-274-272-acc-2" class="collapsed">
<h4 class="panel-title">Why doesn't my stomach feel very good?</h4>
</a>
</div>
<div id="collapse-274-272-acc-2" class="panel-collapse collapse">
<div class="panel-body">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse-274-273-acc-2" class="collapsed">
<h4 class="panel-title">A third FAQ question?</h4>
</a>
</div>
<div id="collapse-274-273-acc-2" class="panel-collapse collapse">
<div class="panel-body">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">faq_accordion</span>
<div class="block clearfix colored faq_accordion">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">FAQ</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-12">
<p><h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p></p>
</div>
<div class="col-md-12">
<div class="panel-group" id="accordion-274">
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse-274-268-acc-3" class="collapsed">
<h4 class="panel-title">Who ate all of my candy?</h4>
</a>
</div>
<div id="collapse-274-268-acc-3" class="panel-collapse collapse">
<div class="panel-body">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse-274-269-acc-3" class="collapsed">
<h4 class="panel-title">Why doesn't my stomach feel very good?</h4>
</a>
</div>
<div id="collapse-274-269-acc-3" class="panel-collapse collapse">
<div class="panel-body">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse-274-270-acc-3" class="collapsed">
<h4 class="panel-title">A third FAQ question?</h4>
</a>
</div>
<div id="collapse-274-270-acc-3" class="panel-collapse collapse">
<div class="panel-body">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse-274-271-acc-3" class="collapsed">
<h4 class="panel-title">Who ate all of my candy?</h4>
</a>
</div>
<div id="collapse-274-271-acc-3" class="panel-collapse collapse">
<div class="panel-body">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse-274-272-acc-3" class="collapsed">
<h4 class="panel-title">Why doesn't my stomach feel very good?</h4>
</a>
</div>
<div id="collapse-274-272-acc-3" class="panel-collapse collapse">
<div class="panel-body">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse-274-273-acc-3" class="collapsed">
<h4 class="panel-title">A third FAQ question?</h4>
</a>
</div>
<div id="collapse-274-273-acc-3" class="panel-collapse collapse">
<div class="panel-body">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section id="text_3_images" class="section">
<span class="label label-default">text_3_images</span>
<div class="block clearfix gray text_3_images">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container no-captions">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-4">
<img class="img-responsive" src="http://placehold.it/360x300" alt="Alt-1" width="340" height="283" pagespeed_url_hash="3092251066"/>
<p class="image-caption"></p>
<div class="column-text">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
<div class="col-md-4">
<img class="img-responsive" src="http://placehold.it/360x300" alt="Alt-1" width="340" height="283" pagespeed_url_hash="3092251066"/>
<p class="image-caption"></p>
<div class="column-text">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
<div class="col-md-4">
<img class="img-responsive" src="http://placehold.it/360x300" alt="Alt-3" width="340" height="283" pagespeed_url_hash="3092251066"/>
<p class="image-caption"></p>
<div class="column-text">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_3_images</span>
<div class="block clearfix colored text_3_images">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container no-captions">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-4">
<img class="img-responsive" src="http://placehold.it/360x300" alt="Alt-1" width="340" height="283" pagespeed_url_hash="3092251066"/>
<p class="image-caption"></p>
<div class="column-text">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
<div class="col-md-4">
<img class="img-responsive" src="http://placehold.it/360x300" alt="Alt-1" width="340" height="283" pagespeed_url_hash="3092251066"/>
<p class="image-caption"></p>
<div class="column-text">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
<div class="col-md-4">
<img class="img-responsive" src="http://placehold.it/360x300" alt="Alt-3" width="340" height="283" pagespeed_url_hash="3092251066"/>
<p class="image-caption"></p>
<div class="column-text">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_3_images</span>
<div class="block clearfix default text_3_images">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container no-captions">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
<p class="block-subtitle">A very long subtitle that we can use for correct positioning</p>
</header>
<div class="row">
<div class="col-md-4">
<img class="img-responsive" src="http://placehold.it/360x300" alt="Alt-1" width="340" height="283" pagespeed_url_hash="3092251066"/>
<p class="image-caption"></p>
<div class="column-text">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
<div class="col-md-4">
<img class="img-responsive" src="http://placehold.it/360x300" alt="Alt-1" width="340" height="283" pagespeed_url_hash="3092251066"/>
<p class="image-caption"></p>
<div class="column-text">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
<div class="col-md-4">
<img class="img-responsive" src="http://placehold.it/360x300" alt="Alt-3" width="340" height="283" pagespeed_url_hash="3092251066"/>
<p class="image-caption"></p>
<div class="column-text">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section id="text_2_images" class="section">
<span class="label label-default">text_2_images</span>
<div class="block clearfix gray text_2_images">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Two images with two columns of text</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-6">
<div class="no-caption">
<img class="img-responsive img-1" src="http://placehold.it/550x250" alt="alt-1" width="540" height="245" pagespeed_url_hash="2695947583"/>
<div class="caption-wrapper">
<p class="image-caption"></p>
</div>
</div>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<div class="no-caption">
<img class="img-responsive img-2" src="http://placehold.it/550x250" alt="alt-2" width="540" height="245" pagespeed_url_hash="2695947583"/>
<div class="caption-wrapper">
<p class="image-caption"></p>
</div>
</div>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
<script type="text/javascript">if(typeof equalizeElms=="undefined")var equalizeElms=new Object();equalizeElms['entry-386']='.image-caption';</script>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_2_images</span>
<div class="block clearfix colored text_2_images">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Two images with two columns of text</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-6">
<div class="no-caption">
<img class="img-responsive img-1" src="http://placehold.it/550x250" alt="alt-1" width="540" height="245" pagespeed_url_hash="2695947583"/>
<div class="caption-wrapper">
<p class="image-caption"></p>
</div>
</div>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<div class="no-caption">
<img class="img-responsive img-2" src="http://placehold.it/550x250" alt="alt-2" width="540" height="245" pagespeed_url_hash="2695947583"/>
<div class="caption-wrapper">
<p class="image-caption"></p>
</div>
</div>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
<script type="text/javascript">if(typeof equalizeElms=="undefined")var equalizeElms=new Object();equalizeElms['entry-386']='.image-caption';</script>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_2_images</span>
<div class="block clearfix default text_2_images">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Two images with two columns of text</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-6">
<div class="no-caption">
<img class="img-responsive img-1" src="http://placehold.it/550x250" alt="alt-1" width="540" height="245" pagespeed_url_hash="2695947583"/>
<div class="caption-wrapper">
<p class="image-caption"></p>
</div>
</div>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<div class="no-caption">
<img class="img-responsive img-2" src="http://placehold.it/550x250" alt="alt-2" width="540" height="245" pagespeed_url_hash="2695947583"/>
<div class="caption-wrapper">
<p class="image-caption"></p>
</div>
</div>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
<script type="text/javascript">if(typeof equalizeElms=="undefined")var equalizeElms=new Object();equalizeElms['entry-386']='.image-caption';</script>
</div>
</div>
</div>
</section>

<section id="table_text_right" class="section">
<span class="label label-default">table_text_right</span>
<div class="block clearfix gray table_text_right">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Playground Table Text on Right</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="table-block col-md-6">
<table class="table table-striped">
<thead>
<tr>
<th>#</th>
<th>First Name</th>
<th>Last Name</th>
<th>Username</th>
</tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
</tbody>
</table>
</div>
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">table_text_right</span>
<div class="block clearfix colored table_text_right">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Playground Table Text on Right</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="table-block col-md-6">
<table class="table table-striped">
<thead>
<tr>
<th>#</th>
<th>First Name</th>
<th>Last Name</th>
<th>Username</th>
</tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
</tbody>
</table>
</div>
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">table_text_right</span>
<div class="block clearfix default table_text_right">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Playground Table Text on Right</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="table-block col-md-6">
<table class="table table-striped">
<thead>
<tr>
<th>#</th>
<th>First Name</th>
<th>Last Name</th>
<th>Username</th>
</tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
</tbody>
</table>
</div>
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section id="table_text_left" class="section">
<span class="label label-default">table_text_left</span>
<div class="block clearfix gray table_text_left">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Table with Text on the Left</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="table-block col-md-6">
<table class="table table-striped">
<thead>
<tr>
<th>#</th>
<th>First Name</th>
<th>Last Name</th>
<th>Username</th>
</tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">table_text_left</span>
<div class="block clearfix colored table_text_left">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Table with Text on the Left</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="table-block col-md-6">
<table class="table table-striped">
<thead>
<tr>
<th>#</th>
<th>First Name</th>
<th>Last Name</th>
<th>Username</th>
</tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">table_text_left</span>
<div class="block clearfix default table_text_left">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Table with Text on the Left</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="table-block col-md-6">
<table class="table table-striped">
<thead>
<tr>
<th>#</th>
<th>First Name</th>
<th>Last Name</th>
<th>Username</th>
</tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section id="table_full_width" class="section">
<span class="label label-default">table_full_width</span>
<div class="block clearfix gray table_full_width">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A full width table</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="table-block">
<table class="table table-striped">
<thead>
<tr>
<th>#</th>
<th>First Name</th>
<th>Last Name</th>
<th>Username</th>
</tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">table_full_width</span>
<div class="block clearfix colored table_full_width">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A full width table</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="table-block">
<table class="table table-striped">
<thead>
<tr>
<th>#</th>
<th>First Name</th>
<th>Last Name</th>
<th>Username</th>
</tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">table_full_width</span>
<div class="block clearfix default table_full_width">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A full width table</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="table-block">
<table class="table table-striped">
<thead>
<tr>
<th>#</th>
<th>First Name</th>
<th>Last Name</th>
<th>Username</th>
</tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
<tr>
<td>1</td>
<td>Mark</td>
<td>Otto</td>
<td>@mdo</td>
</tr>
<tr>
<td>2</td>
<td>Jacob</td>
<td>Thornton</td>
<td>@fat</td>
</tr>
<tr>
<td>3</td>
<td>Larry</td>
<td>the Bird</td>
<td>@twitter</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<!--
<section id="slider_two_column_text" class="section">
<span class="label label-default">slider_two_column_text</span>
<div class="block clearfix gray slider_two_column_text">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Careers</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row carousel">
<div class="col-md-12 carousel">
<div id="carousel-293-10" class="royalSlider rsDefault ">
<img class="rsImg" src="http://placehold.it/1131x250" alt="Alt-1" width="1140" height="252" pagespeed_url_hash="351548600"/>
<img class="rsImg" src="http://placehold.it/1131x250" alt="Alt-2" width="1140" height="252" pagespeed_url_hash="351548600"/>
<img class="rsImg" src="http://placehold.it/1131x250" alt="Alt-3" width="1140" height="252" pagespeed_url_hash="351548600"/>
</div>
</div>
</div>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
<script type="text/javascript">if(typeof blockSliders=="undefined")var blockSliders=new Object();blockSliders['carousel-293-10']={};</script>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">slider_two_column_text</span>
<div class="block clearfix colored slider_two_column_text">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Careers</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row carousel">
<div class="col-md-12 carousel">
<div id="carousel-293-11" class="royalSlider rsDefault ">
<img class="rsImg" src="http://placehold.it/1131x250" alt="Alt-1" width="1140" height="252" pagespeed_url_hash="351548600"/>
<img class="rsImg" src="http://placehold.it/1131x250" alt="Alt-2" width="1140" height="252" pagespeed_url_hash="351548600"/>
<img class="rsImg" src="http://placehold.it/1131x250" alt="Alt-3" width="1140" height="252" pagespeed_url_hash="351548600"/>
</div>
</div>
</div>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
<script type="text/javascript">if(typeof blockSliders=="undefined")var blockSliders=new Object();blockSliders['carousel-293-11']={};</script>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">slider_two_column_text</span>
<div class="block clearfix default slider_two_column_text">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Careers</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row carousel">
<div class="col-md-12 carousel">
<div id="carousel-293-12" class="royalSlider rsDefault ">
<img class="rsImg" src="http://placehold.it/1131x250" alt="Alt-1" width="1140" height="252" pagespeed_url_hash="351548600"/>
<img class="rsImg" src="http://placehold.it/1131x250" alt="Alt-2" width="1140" height="252" pagespeed_url_hash="351548600"/>
<img class="rsImg" src="http://placehold.it/1131x250" alt="Alt-3" width="1140" height="252" pagespeed_url_hash="351548600"/>
</div>
</div>
</div>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
<script type="text/javascript">if(typeof blockSliders=="undefined")var blockSliders=new Object();blockSliders['carousel-293-12']={};</script>
</div>
</div>
</div>
</div>
</section>
-->
<section id="image_text_overlay" class="section">
<span class="label label-default">image_text_overlay</span>
<div class="block clearfix gray image_text_overlay">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<div class="row">
<div class="col-md-12 text-overlay-container">
<img class="img-responsive" src="http://placehold.it/1131x500" alt="full-width-image" width="1140" height="504" pagespeed_url_hash="18881434"/>
<div class="text-overlay">
<header class="block-header clearfix">
<h3 class="block-title">An image with a text overlay different from super head</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="text-overlay-lead lead"><h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">image_text_overlay</span>
<div class="block clearfix colored image_text_overlay">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<div class="row">
<div class="col-md-12 text-overlay-container">
<img class="img-responsive" src="http://placehold.it/1131x500" alt="full-width-image" width="1140" height="504" pagespeed_url_hash="18881434"/>
<div class="text-overlay">
<header class="block-header clearfix">
<h3 class="block-title">An image with a text overlay different from super head</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="text-overlay-lead lead"><h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">image_text_overlay</span>
<div class="block clearfix default image_text_overlay">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<div class="row">
<div class="col-md-12 text-overlay-container">
<img class="img-responsive" src="http://placehold.it/1131x500" alt="full-width-image" width="1140" height="504" pagespeed_url_hash="18881434"/>
<div class="text-overlay">
<header class="block-header clearfix">
<h3 class="block-title">An image with a text overlay different from super head</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="text-overlay-lead lead"><h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section id="image_gallery" class="section">
<span class="label label-default">image_gallery</span>
<div class="block clearfix gray image_gallery">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">An image gallery block with six pictures</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-1" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-2" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-3" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-4" pagespeed_url_hash="3785462749"/>
</div>
</div>
<div class="row">
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-5" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-6" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-7" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-8" pagespeed_url_hash="3785462749"/>
</div>
</div>
<div class="row">
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-9" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-10" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-11" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-12" pagespeed_url_hash="3785462749"/>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">image_gallery</span>
<div class="block clearfix colored image_gallery">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">An image gallery block with six pictures</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-1" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-2" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-3" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-4" pagespeed_url_hash="3785462749"/>
</div>
</div>
<div class="row">
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-5" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-6" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-7" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-8" pagespeed_url_hash="3785462749"/>
</div>
</div>
<div class="row">
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-9" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-10" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-11" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-12" pagespeed_url_hash="3785462749"/>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">image_gallery</span>
<div class="block clearfix default image_gallery">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">An image gallery block with six pictures</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-1" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-2" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-3" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-4" pagespeed_url_hash="3785462749"/>
</div>
</div>
<div class="row">
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-5" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-6" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-7" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-8" pagespeed_url_hash="3785462749"/>
</div>
</div>
<div class="row">
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-9" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-10" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-11" pagespeed_url_hash="3785462749"/>
</div>
<div class="col-lg-3 col-sm-6 col-xs-6 col-md-3">
<img class="img-responsive" src="http://placehold.it/360x300" alt="alt-12" pagespeed_url_hash="3785462749"/>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section id="image_full_width" class="section">
<span class="label label-default">image_full_width</span>
<div class="block clearfix gray image_full_width">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Image with non background full width</h3>
</header>
<div class="row">
<div class="col-md-12">
<img class="img-responsive" src="http://placehold.it/1131x500" alt="full-width-image" width="1140" height="504" pagespeed_url_hash="18881434"/>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">image_full_width</span>
<div class="block clearfix colored image_full_width">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Image with non background full width</h3>
</header>
<div class="row">
<div class="col-md-12">
<img class="img-responsive" src="http://placehold.it/1131x500" alt="full-width-image" width="1140" height="504" pagespeed_url_hash="18881434"/>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">image_full_width</span>
<div id="entry-383-5" class="block clearfix default image_full_width">
<div class="row block-row">
<div class="block-container col-md-12">
<style>#entry-383-5{height:1028px}</style>
<script type="text/javascript">
$("#entry-383-5").backstretch("http://calvarylife/wp-content/themes/calvarylife-ca-roots/assets/img/cl-wc.jpg");
</script>
</div>
</div>
</div>
</section>

<section id="graph_text_right" class="section">
<span class="label label-default">graph_text_right</span>
<div class="block clearfix gray graph_text_right">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Text with an image or graph on the left</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-6 graph-container">
<img class="img-responsive graph" src="http://placehold.it/550x400" alt="graph-right" width="540" height="393" pagespeed_url_hash="2022923344"/>
</div>
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">graph_text_right</span>
<div class="block clearfix colored graph_text_right">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Text with an image or graph on the left</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-6 graph-container">
<img class="img-responsive graph" src="http://placehold.it/550x400" alt="graph-right" width="540" height="393" pagespeed_url_hash="2022923344"/>
</div>
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">graph_text_right</span>
<div class="block clearfix default graph_text_right">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Text with an image or graph on the left</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-6 graph-container">
<img class="img-responsive graph" src="http://placehold.it/550x400" alt="graph-right" width="540" height="393" pagespeed_url_hash="2022923344"/>
</div>
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section id="graph_text_left" class="section">
<span class="label label-default">graph_text_left</span>
<div class="block clearfix gray graph_text_left">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Text with image or a graph on the right</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6 graph-container">
<img class="img-responsive graph" src="http://placehold.it/550x400" alt="graph-left" width="540" height="393" pagespeed_url_hash="2022923344"/>

</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">graph_text_left</span>
<div class="block clearfix colored graph_text_left">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Text with image or a graph on the right</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6 graph-container">
<img class="img-responsive graph" src="http://placehold.it/550x400" alt="graph-left" width="540" height="393" pagespeed_url_hash="2022923344"/>

</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">graph_text_left</span>
<div class="block clearfix default graph_text_left">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">Text with image or a graph on the right</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-6">
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<h5>PARAGRAPH TITLE</h5>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
<p>Aoluptata volut voluptur. Parum iunt rerfere nos magniet remos atibusci quam quo cus, am nat facerib usaepudicae milit quunt et lantionsequi unt ra quod magnisquae sitaeperiae voluptat. Cus solupta nist omnim idellorro conserum que voluptissimo berae sus cumquuntur aut mosam quissusame nectus am volora con restempelit, ommos ulluptio et idelis qui duciam raeprest, ut labo.</p>
</div>
<div class="col-md-6 graph-container">
<img class="img-responsive graph" src="http://placehold.it/550x400" alt="graph-left" width="540" height="393" pagespeed_url_hash="2022923344"/>

</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section id="graph_full_width" class="section">
<span class="label label-default">graph_full_width</span>
<div class="block clearfix gray graph_full_width">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A full width image or graph</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-12 graph-container">
<img class="img-responsive graph" src="http://placehold.it/1131x500" alt="full-width-image" width="1140" height="504" pagespeed_url_hash="18881434"/>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">graph_full_width</span>
<div class="block clearfix colored graph_full_width">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A full width image or graph</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-12 graph-container">
<img class="img-responsive graph" src="http://placehold.it/1131x500" alt="full-width-image" width="1140" height="504" pagespeed_url_hash="18881434"/>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">graph_full_width</span>
<div class="block clearfix default graph_full_width">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A full width image or graph</h3>
<p class="block-subtitle">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
</header>
<div class="row">
<div class="col-md-12 graph-container">
<img class="img-responsive graph" src="http://placehold.it/1131x500" alt="full-width-image" width="1140" height="504" pagespeed_url_hash="18881434"/>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section id="cta_phone" class="section">
<span class="label label-default">cta_phone</span>
<div class="block clearfix gray cta_phone">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
</header>
<p class="block-subtitle-plus">A very long subtitle that we can use for correct positioning</p>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">cta_phone</span>
<div class="block clearfix colored cta_phone">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
</header>
<p class="block-subtitle-plus">A very long subtitle that we can use for correct positioning</p>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">cta_phone</span>
<div class="block clearfix default cta_phone">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<header class="block-header clearfix">
<h3 class="block-title">A very long playground title</h3>
</header>
<p class="block-subtitle-plus">A very long subtitle that we can use for correct positioning</p>
</div>
</div>
</div>
</div>
</section>

<section id="cta_form" class="section section_hidden">
<span class="label label-default">cta_form</span>
<div class="block clearfix gray cta_form">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<div class="row">
<div class="table-block">
<div class="cta-container center-block">
<header class="block-header">
<h3 class="block-title">A very long playground title</h3>
</header>
<form action="" accept-charset="utf-8" class="form-inline call-to-action-form ajax-form" method="post"><div style="display:none">
<input type="hidden" name="params_id" value="5230"/>
<input type="hidden" name="XID" value="874a5a049aa0ae4c9cedf92f123ca6b2a925f8b8"/>
</div>
<input type="text" name="full_name" value="" id="cta-full-name" maxlength="256" class="cta-field form-control" placeholder="Full Name"/>
<input type="text" name="email" value="" id="cta-email" maxlength="150" class="cta-field form-control" placeholder="E-mail address"/>
<input type="text" name="phone_number" value="" id="cta-phone" maxlength="256" class="cta-field form-control" placeholder="Phone Number"/>
<input type="hidden" value="Playground" name="custom_page_title">
<input type="hidden" value="" name="custom_page_url">
<input type="submit" name="submit" value="submit" class="submit-form-manual"/>
<i class="fa fa-chevron-right"></i>
<div class="callback"></div>
<div class="snap" style="position: absolute !important; top: -10000px !important;"><input type="text" name="snap_ZaGQYkcfA" value="JrDfHdbnFEFoH"/></div>
<script type="text/javascript">$(function(){$("input[name='snap_ZaGQYkcfA']").val('WriABKPEQtgTA');});</script>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">cta_form</span>
<div class="block clearfix colored cta_form">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<div class="row">
<div class="table-block">
<div class="cta-container center-block">
<header class="block-header">
<h3 class="block-title">A very long playground title</h3>
</header>
<form action="" accept-charset="utf-8" class="form-inline call-to-action-form ajax-form" method="post"><div style="display:none">
<input type="hidden" name="params_id" value="5230"/>
<input type="hidden" name="XID" value="874a5a049aa0ae4c9cedf92f123ca6b2a925f8b8"/>
</div>
<input type="text" name="full_name" value="" id="cta-full-name" maxlength="256" class="cta-field form-control" placeholder="Full Name"/>
<input type="text" name="email" value="" id="cta-email" maxlength="150" class="cta-field form-control" placeholder="E-mail address"/>
<input type="text" name="phone_number" value="" id="cta-phone" maxlength="256" class="cta-field form-control" placeholder="Phone Number"/>
<input type="hidden" value="Playground" name="custom_page_title">
<input type="hidden" value="" name="custom_page_url">
<input type="submit" name="submit" value="submit" class="submit-form-manual"/>
<i class="fa fa-chevron-right"></i>
<div class="callback"></div>
<div class="snap" style="position: absolute !important; top: -10000px !important;"><input type="text" name="snap_ZaGQYkcfA" value="JrDfHdbnFEFoH"/></div>
<script type="text/javascript">$(function(){$("input[name='snap_ZaGQYkcfA']").val('WriABKPEQtgTA');});</script>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">cta_form</span>
<div class="block clearfix default cta_form">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<div class="row">
<div class="table-block">
<div class="cta-container center-block">
<header class="block-header">
<h3 class="block-title">A very long playground title</h3>
</header>
<form action="" accept-charset="utf-8" class="form-inline call-to-action-form ajax-form" method="post"><div style="display:none">
<input type="hidden" name="params_id" value="5230"/>
<input type="hidden" name="XID" value="874a5a049aa0ae4c9cedf92f123ca6b2a925f8b8"/>
</div>
<input type="text" name="full_name" value="" id="cta-full-name" maxlength="256" class="cta-field form-control" placeholder="Full Name"/>
<input type="text" name="email" value="" id="cta-email" maxlength="150" class="cta-field form-control" placeholder="E-mail address"/>
<input type="text" name="phone_number" value="" id="cta-phone" maxlength="256" class="cta-field form-control" placeholder="Phone Number"/>
<input type="hidden" value="Playground" name="custom_page_title">
<input type="hidden" value="" name="custom_page_url">
<input type="submit" name="submit" value="submit" class="submit-form-manual"/>
<i class="fa fa-chevron-right"></i>
<div class="callback"></div>
<div class="snap" style="position: absolute !important; top: -10000px !important;"><input type="text" name="snap_ZaGQYkcfA" value="JrDfHdbnFEFoH"/></div>
<script type="text/javascript">$(function(){$("input[name='snap_ZaGQYkcfA']").val('WriABKPEQtgTA');});</script>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section id="cta_button" class="section section_hidden">
<span class="label label-default">cta_button</span>
<div class="block clearfix gray cta_button">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<div class="row">
<div class="table-block">
<div class="cta-container center-block clearfix">
<header class="block-header">
<h3 class="block-title">A very long playground title</h3>
</header>
<a class="btn btn-primary cta-btn" href="http://www.google.com" target="_blank">GO TO WEBSITE</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">cta_button</span>
<div class="block clearfix colored cta_button">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<div class="row">
<div class="table-block">
<div class="cta-container center-block clearfix">
<header class="block-header">
<h3 class="block-title">A very long playground title</h3>
</header>
<a class="btn btn-primary cta-btn" href="http://www.google.com" target="_blank">GO TO WEBSITE</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">cta_button</span>
<div class="block clearfix default cta_button">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<div class="row">
<div class="table-block">
<div class="cta-container center-block clearfix">
<header class="block-header">
<h3 class="block-title">A very long playground title</h3>
</header>
<a class="btn btn-primary cta-btn" href="http://www.google.com" target="_blank">GO TO WEBSITE</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section id="text_super_banner" class="section">
<span class="label label-default">text_super_banner</span>
<div class="block clearfix gray text_super_banner">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="super-banner" id="banner-384-1"></div>
<div class="container">
<h2 class="block-title-plus">Title for the text super head banner</h2>
<p class="block-subtitle-plus">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
<p class="lead">
This is lead copy Sed eleifund metus ut nisi sollicitudin eusmod a in metus. Aliquam sed mi porta mi congue ullamcorper. Phasellus rutrum lectus sit amet nisl faucibus adipiscing aoluptat voluptat.
</p>
</div>
<script type="text/javascript">
$("#banner-384-1").backstretch("/wp-content/themes/calvarylife-ca-roots/assets/img/cl-wc.jpg");
</script>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_super_banner</span>
<div class="block clearfix colored text_super_banner">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="super-banner" id="banner-384-2"></div>
<div class="container">
<h2 class="block-title-plus">Title for the text super head banner</h2>
<p class="block-subtitle-plus">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
<p class="lead">
This is lead copy Sed eleifund metus ut nisi sollicitudin eusmod a in metus. Aliquam sed mi porta mi congue ullamcorper. Phasellus rutrum lectus sit amet nisl faucibus adipiscing aoluptat voluptat.
</p>
</div>
<script type="text/javascript">
$("#banner-384-2").backstretch("/wp-content/themes/calvarylife-ca-roots/assets/img/cl-wc.jpg");
</script>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_super_banner</span>
<div class="block clearfix default text_super_banner">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="super-banner" id="banner-384-3"></div>
<div class="container">
<h2 class="block-title-plus">Title for the text super head banner</h2>
<p class="block-subtitle-plus">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
<p class="lead">
This is lead copy Sed eleifund metus ut nisi sollicitudin eusmod a in metus. Aliquam sed mi porta mi congue ullamcorper. Phasellus rutrum lectus sit amet nisl faucibus adipiscing aoluptat voluptat.
</p>
</div>
<script type="text/javascript">
$("#banner-384-3").backstretch("/wp-content/themes/calvarylife-ca-roots/assets/img/cl-wc.jpg");
</script>
</div>
</div>
</div>
</section>

<section id="text_super" class="section">
<span class="label label-default">text_super</span>
<div class="block clearfix gray text_super">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<h2 class="block-title-plus">This long title is here to be a long title</h2>
<p class="block-subtitle-plus">THIS IS THE SUB-TITLE FOR THIS WEBSITE PAGE</p>
<p class="lead">
This is lead copy Sed eleifund metus ut nisi sollicitudin eusmod a in metus. Aliquam sed mi porta mi congue ullamcorper. Phasellus rutrum lectus sit amet nisl faucibus adipiscing aoluptat voluptat.
</p>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_super</span>
<div class="block clearfix colored text_super">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<h2 class="block-title-plus">This long title is here to be a long title</h2>
<p class="block-subtitle-plus">THIS IS THE SUB-TITLE FOR THIS WEBSITE PAGE</p>
<p class="lead">
This is lead copy Sed eleifund metus ut nisi sollicitudin eusmod a in metus. Aliquam sed mi porta mi congue ullamcorper. Phasellus rutrum lectus sit amet nisl faucibus adipiscing aoluptat voluptat.
</p>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_super</span>
<div class="block clearfix default text_super">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<h2 class="block-title-plus">This long title is here to be a long title</h2>
<p class="block-subtitle-plus">THIS IS THE SUB-TITLE FOR THIS WEBSITE PAGE</p>
<p class="lead">
This is lead copy Sed eleifund metus ut nisi sollicitudin eusmod a in metus. Aliquam sed mi porta mi congue ullamcorper. Phasellus rutrum lectus sit amet nisl faucibus adipiscing aoluptat voluptat.
</p>
</div>
</div>
</div>
</div>
</section>
<section class="section section_hidden">
<span class="label label-default">text_super</span>
<div id="entry-385-4" class="block clearfix default text_super backstretch">
<div class="row block-row">
<div class="block-container col-md-12">
<div class="container">
<h2 class="block-title-plus">Playground Text Super Head</h2>
<p class="block-subtitle-plus">PARUM IUNT RERFERE NOS MAGNIET REMO.</p>
<p class="lead">
This is lead copy Sed eleifund metus ut nisi sollicitudin eusmod a in metus. Aliquam sed mi porta mi congue ullamcorper. Phasellus rutrum lectus sit amet nisl faucibus adipiscing aoluptat voluptat.
</p>
</div>
<script type="text/javascript">
$("#entry-385-4").backstretch("http://calvarylife/wp-content/themes/calvarylife-ca-roots/assets/img/cl-wc.jpg");
</script>
</div>
</div>
</div>
</section>