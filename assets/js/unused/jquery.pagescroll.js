var pageExtended = {

    init: function() {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        this.vars.sidebar = $("#sidebar-content");
        this.vars.sections = $('.section:not(.section_hidden)');
        this.vars.firstSectionId = this.vars.sections.first().attr('id');
        formHelper.init();
        backstrechHelper.init();
        equalizerHelper.init();
        navigationHelper.init();
        mapHelper.init();
        sliderHelper.init();
        $(window).load(function() {
            document.body.scrollTop = document.documentElement.scrollTop = 0;
            pageExtended.scrollAction(window.location.hash, false);
        });
        this.listeners();
    },

    listeners: function() {
        $("a[href^=#]:not(.carousel-control)").click(function (f) {
            if (typeof $(this).data('toggle') != "undefined") return;
            f.preventDefault();
            pageExtended.scrollAction($(this).attr("href"), true);
            $(this).blur();
            return true;
        });
        pageExtended.vars.sidebar.affix({
            offset: {
                top: 100,
                bottom: 100
            }
        });
        this.vars.sections.waypoint(function(direction) {
                pageExtended.scrollRefresh(direction,  $(this).attr('id'));
                pageExtended.vars.lastSection = false;
            },
            {
                offset: function() {
                    return '110';
                }
            }
        );
        $(this.vars.sections[1]).waypoint(function(direction) {

                if (direction == "down")
                    $('.navbar').addClass('navbar-small');
                else
                    $('.navbar').removeClass('navbar-small');

            },
            {
                offset: function() {
                    return '110';
                }
            }
        );
        var lazyResize = _.debounce(function() {
            equalizerHelper.refresh();
            backstrechHelper.refresh();
        }, 50);


        $(window).resize(function() {
            lazyResize();
        });

        var lazyScrollSpy = _.debounce(function() {
            if($(window).scrollTop() + $(window).height() > $(document).height() - 10) {
                var lastSection =  pageExtended.vars.sections.last();
                if (lastSection.hasClass('section_hidden')) lastSection = lastSection.prev();
                pageExtended.scrollRefresh("down", lastSection.attr('id'));
                pageExtended.vars.lastSection = true;
            } else if (pageExtended.vars.lastSection == true) {
                var lastSection =  pageExtended.vars.sections.last();
                if (lastSection.hasClass('section_hidden')) lastSection = lastSection.prev();
                pageExtended.scrollRefresh("up", lastSection.attr('id'));
                pageExtended.vars.lastSection = false;
            }
        }, 50);
        $(window).scroll(function () {
            lazyScrollSpy();
        });

        $(window).load(function() {
            $(window).resize();
        });

    },

    scrollAction: function(d, change) {
        if (d === "" || d === "#") {
            if (pageExtended.vars.loaded == false) pageExtended.vars.loaded = true;
            return;
        }
        var speed = 1500;
        d = d.slice(d.indexOf("#") + 1);
        var scrollPos = 0;
        if (this.vars.firstSectionId == d) {
            scrollPos = 0;
        }
        else if ($("#navbar").css("position") == "relative") {
        	scrollPos = d === "top" ? 0 : $("#" + d).offset().top // < 992px no longer has a fixed top navbar so it does not need an offest
        }
        else
            scrollPos = d === "top" ? 0 : $("#" + d).offset().top - 78 // 80 - 2 of padding just in case;
        $("html, body").stop(true).animate({
            scrollTop: scrollPos
        }, speed, function () {
            if (pageExtended.vars.loaded == false) pageExtended.vars.loaded = true;
        });
    },

    scrollRefresh: function(direction, id) {
        var elm = pageExtended.vars.sidebar.find('a[href="#'+ id +'"]');
        if (elm.length == 0) return;
        var current = elm.parent();
        var prev = current.prev();
        if (direction != "down" && prev.length != 0) current = prev;
        if(history.pushState && pageExtended.vars.loaded == true) {
            var link = current.find('a').attr('href');
            if (typeof link != "undefined" && link != "") history.pushState(null, null, current.find('a').attr('href'));
        }
        pageExtended.vars.sidebar.find('li').not(current).removeClass('active');
        current.addClass('active');
    },

    vars: {
        sections: null,
        sidebar: null,
        lastSection: false,
        firstSectionId: null,
        loaded: false
    }
};
