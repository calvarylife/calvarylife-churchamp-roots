var backstrechHelper = {

    init: function() {
        if (typeof backstrechImgs != "undefined") {
            _.each(backstrechImgs, function(url, index) {
                var obj = $("#"+index);
                backstrechHelper.vars.objects.push(obj);
                $(obj).backstretch(url);
            });
        }
    },

    refresh: function() {
        if (typeof backstrechImgs != "undefined") {
            _.each(backstrechHelper.vars.objects, function(obj) {
                $(obj).backstretch("resize");
            });
        }
    },

    vars: {
        objects:[]
    }
};