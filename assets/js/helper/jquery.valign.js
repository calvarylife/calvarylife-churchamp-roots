var dynamicAlign = {

    init: function() {
        this.vars.objects = $('.dynamicAlign');
    },

    refresh: function() {
        _.each( dynamicAlign.vars.objects, function(obj) {
            var newVars = {};
            var parent = typeof $(obj).data('vParent') != "undefined" ? $($(obj).data('vParent')) : $(obj).parent();

            if ($(this).data('vVertical') != 'false') {
                newVars['margin-top'] = (parent.outerHeight() / 2) - ($(obj).height() / 2);
                newVars['margin-top']  = newVars['margin-top'] < 0 ? 0 : newVars['margin-top'];
            }

            if ($(this).data('vHorizontal') == 'false') {
                newVars['margin-left'] = (parent.width() / 2) - ($(obj).width() / 2);
                newVars['margin-left']  = newVars['margin-left'] < 0 ? 0 : newVars['margin-left'];
            }
            $(obj).animate(newVars, 0);
        });
    },

    vars: {
        objects:[]
    }
};