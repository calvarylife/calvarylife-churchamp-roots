<?php
/**
 * Custom functions
 */

// Register Theme Scripts
function theme_scripts() {
  wp_register_script('helper', get_template_directory_uri() . '/assets/js/minified/helper.min.js', false, null, false);
  wp_register_script('vendor', get_template_directory_uri() . '/assets/js/minified/vendor.min.js', false, null, false);
  wp_register_script('gmaps', get_template_directory_uri() . '/assets/js/minified/gmaps.min.js', false, null, false);
  wp_enqueue_script('helper');
  wp_enqueue_script('vendor');
  wp_enqueue_script('gmaps');
}
add_action('wp_enqueue_scripts', 'theme_scripts', 100);

// Regsiter FontAwesome
function fontawesome_script() {
	wp_enqueue_style( 'fa-style', '//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css', array(), '1.0', 'screen,projection');
}
add_action('wp_enqueue_scripts', 'fontawesome_script');

// Register Symbolset
function symbolset_script() {
	wp_enqueue_script( 'ss_social_script', get_stylesheet_directory_uri() . '/assets/fonts/symbolset/ss-social-regular.js', array('jquery'), '', true );
	wp_enqueue_style( 'ss_social_style', get_stylesheet_directory_uri() . '/assets/fonts/symbolset/ss-social-regular.css', array(), '1.0', 'screen,projection');
}
add_action('wp_enqueue_scripts', 'symbolset_script');

// Register Tiny MCE Buttons
function enable_more_buttons($buttons) {
  $buttons[] = 'hr';
 return $buttons;
}
add_filter("mce_buttons", "enable_more_buttons");

// ACF - Custom Styles

function my_acf_admin_head()
{
	?>
	<style type="text/css">

		table.acf_input > tbody > tr[data-field_name="block"] > td.label,
		table.acf_input > tbody > tr[data-field_name="block_column"] > td.label {
			display:none;
		}

		.repeater > table.acf-input-table > tbody > tr > td {
			border-bottom:1px solid #288cb9;
		}

		.repeater > table > tbody > tr > td.order {
			vertical-align:top;
			padding-top:15px;
			font-weight:bold;
			color:#000;
		}

	</style>
	<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');

// ACF - Gravity Forms Field

// DISABLE HEARTBEAT API
add_action( 'init', 'my_deregister_heartbeat', 1 );
function my_deregister_heartbeat() {
	global $pagenow;

	if ( 'post.php' != $pagenow && 'post-new.php' != $pagenow )
		wp_deregister_script('heartbeat');
}


// Page Builder
function mytheme_panels_row_styles($styles) {
    $styles['verb-theme'] = __('Verb Theme', 'endeavr');
    return $styles;
}
add_filter('siteorigin_panels_row_styles', 'mytheme_panels_row_styles');


?>