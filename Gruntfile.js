'use strict';
module.exports = function(grunt) {

  grunt.initConfig({
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      all: [
        'Gruntfile.js',
        'assets/js/*.js',
        '!asssets/js/plugins/*.js',
        '!asssets/js/bootstrap/*.js',
        '!asssets/js/vendor/*.js',
        '!asssets/js/ie/*.js',
        '!assets/js/minified/*.js'
      ]
    },
    less: {
      dist: {
        files: {
          'assets/css/main.min.css': [
            'assets/less/app.less'
          ]
        },
        options: {
          compress: true,
          // LESS source map
          // To enable, set sourceMap to true and update sourceMapRootpath based on your install
          sourceMap: false,
          sourceMapFilename: 'assets/css/main.min.css.map',
          sourceMapRootpath: '/app/themes/roots/'
        }
      }
    },
    uglify: {
      dist: {
        files: {
          'assets/js/minified/scripts.min.js': [
						'assets/js/plugins/bootstrap/transition.js',
            'assets/js/plugins/bootstrap/alert.js',
            'assets/js/plugins/bootstrap/button.js',
            'assets/js/plugins/bootstrap/carousel.js',
            'assets/js/plugins/bootstrap/collapse.js',
            'assets/js/plugins/bootstrap/dropdown.js',
            'assets/js/plugins/bootstrap/modal.js',
            'assets/js/plugins/bootstrap/tooltip.js',
            'assets/js/plugins/bootstrap/popover.js',
            'assets/js/plugins/bootstrap/scrollspy.js',
            'assets/js/plugins/bootstrap/tab.js',
            'assets/js/plugins/bootstrap/affix.js',
            'assets/js/plugins/*.js',
            'assets/js/_*.js'
          ]
        },
        options: {
          // JS source map: to enable, uncomment the lines below and update sourceMappingURL based on your install
          // sourceMap: 'assets/js/scripts.min.js.map',
          // sourceMappingURL: '/app/themes/roots/assets/js/scripts.min.js.map'
        }
      }
    },
    concat: {
        options: {
            separator: ';'
        },
        scripts: {
            files: [
                {src: [
                    'assets/js/helper/*.js'
                ], dest: 'assets/js/minified/helper.min.js'},
                {src: [
                    'assets/js/vendor/backstretch/*.js',
				            'assets/js/vendor/equalizer/*.js',
				            'assets/js/vendor/placeholder/*.js',
				            'assets/js/vendor/respond/*.js',
				            'assets/js/vendor/royalslider/*.js',
				            'assets/js/vendor/underscore/*.js',
				            'assets/js/vendor/underscore.string/*.js',
										'assets/js/vendor/waypoints/*.js'
                ], dest: 'assets/js/minified/vendor.min.js'},
                // GOOGLE MAP
                {src: [
                    'assets/js/vendor/gmaps/*.js'
                ], dest: 'assets/js/minified/gmaps.min.js'},
                // IE ONLY
                {src: [
                    'assets/js/ie/html5shiv/*.js',
                    'assets/js/ie/selectivizr/*.js',
                    'assets/js/ie/respond/*.js',
                    'assets/js/ie/theme.ie.js'
                ], dest: 'assets/js/minified/ie.min.js'},
            ]
        }
    },
    version: {
      options: {
        file: 'lib/scripts.php',
        css: 'assets/css/main.min.css',
        cssHandle: 'roots_main',
        js: 'assets/js/minified/scripts.min.js',
        jsHandle: 'roots_scripts'
      }
    },
    watch: {
      less: {
        files: [
          'assets/less/*.less',
          'assets/less/bootstrap/*.less',
          'assets/less/blocks/*.less',
          'assets/less/components/*.less',
          'assets/less/grid/*.less',
          'assets/less/ie/*.less',
					'assets/less/mixins/*.less',
					'assets/less/mq/*.less',
					'assets/less/pages/*.less',
					'assets/less/themes/*.less',
          'assets/less/variables/*.less',
          'assets/less/viewport/*.less'
        ],
        tasks: ['less', 'version']
      },
      js: {
        files: [
          '<%= jshint.all %>'
        ],
        tasks: ['jshint', 'uglify', 'concat', 'version']
      },
      livereload: {
        // Browser live reloading
        // https://github.com/gruntjs/grunt-contrib-watch#live-reloading
        options: {
          livereload: true
        },
        files: [
          'assets/css/main.min.css',
          'assets/js/minified/*.js',
          'templates/*.php',
          '*.php'
        ]
      }
    },
    clean: {
      dist: [
        'assets/css/main.min.css',
        'assets/js/minified/scripts.min.js'
      ]
    }
  });

  // Load tasks
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-wp-version');

  // Register tasks
  grunt.registerTask('default', [
    'clean',
    'less',
    'uglify',
    'concat',
    'version',
    'watch'
  ]);
  grunt.registerTask('dev', [
    //'watch'
  ]);

};
